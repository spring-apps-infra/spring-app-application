package demo.app.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import com.netflix.discovery.shared.Applications;

import demo.app.http.HttpClient;

@RestController
@RequestMapping("/eureka")
public class EurekaDemoController {

    private static final Logger LOG = LoggerFactory.getLogger(EurekaDemoController.class);
    private final EurekaClient discoveryClient;

    protected EurekaDemoController(EurekaClient discoveryClient) {
        this.discoveryClient = discoveryClient;
    }

    @GetMapping(value = "/applications/{appName}/next", produces = "application/json")
    public Object getNextInstance(@PathVariable String appName) {
        final InstanceInfo instance;
        try {
            instance = discoveryClient.getNextServerFromEureka(appName, false);
        } catch (Exception e) {
            LOG.error("Error calling Discovery client : {}", e.getMessage());
            return "{\"error\" : \"" + e.getMessage() + "\"}";
        }
        return instance;
    }

    @GetMapping(value = "/applications", produces = "application/json")
    public Applications getApplications() {
        LOG.info("Called get all apps");
        return discoveryClient.getApplications();
    }

    @GetMapping(value = "/applications/{appName}/next/echo", produces = "application/json")
    public Object callEchoOnApp(@PathVariable String appName) {
        Object nextInstance = getNextInstance(appName);
        if (nextInstance instanceof InstanceInfo instance) {
            LOG.info("Found instance by appname \"{}\"", appName);
            return requestGetOnApp(instance, "/echo");
        }
        LOG.warn("No instance found by appname \"{}\"", appName);
        return nextInstance;
    }

    private static Object requestGetOnApp(InstanceInfo instance, String path) {
        final String url = String.format("http://%s:%s%s", instance.getIPAddr(), instance.getPort(), path);
        LOG.info("Try to call {}", url);
        final String response = HttpClient.get(url);
        LOG.info("Response : {}", response);
        return String.format("{\"calledInstanceId\" : \"%s\", \"calledInstanceUrl\" : \"%s\", \"response\" : %s}", instance.getId(), url, response);
    }
}
