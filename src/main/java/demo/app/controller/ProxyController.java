package demo.app.controller;

import java.util.Base64;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import demo.app.http.HttpClient;

@RestController
@RequestMapping("/proxy")
public class ProxyController {

    private static final Logger LOG = LoggerFactory.getLogger(ProxyController.class);

    @GetMapping(value = "/json", produces = "application/json")
    public String doProxyJson(@RequestParam String base64Url) {
        return contentAsString(base64Url);
    }

    @GetMapping(value = "/html", produces = "text/html")
    public String doProxyHtml(@RequestParam String base64Url) {
        return contentAsString(base64Url);
    }

    private String contentAsString(String base64Url) {
        String url = null;
        try {
            url = new String(Base64.getDecoder().decode(base64Url.getBytes()));
        } catch (Exception e) {
            LOG.error("Error getting Base64 decode {} due to : {}", base64Url, e.getMessage());
            return e.getMessage();
        }
        return HttpClient.get(url);
    }
}
