package demo.app.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/config")
public class ConfigDemoController {

    @Value("${spring.application.name:n/a}")
    private String appName;

    @Value("${demo.app.global:n/a}")
    private String global;

    @Value("${demo.app.changeable:n/a}")
    private String changeable;

    @Value("${demo.app.environment:n/a}")
    private String environment;

    @Value("${demo.app.appspecific:n/a}")
    private String appspecific;

    @GetMapping(value = "/vars", produces = "application/json")
    public String configVars() {
        return "{\"global\" : \"" + global + "\", \"changeable\" : \"" + changeable + "\" , \"environment\" : \""
                + environment + "\", \"appspecific\" : \"" + appspecific + "\"}";
    }
}
