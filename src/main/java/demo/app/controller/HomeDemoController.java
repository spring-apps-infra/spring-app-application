package demo.app.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeDemoController {

    @Value("${spring.application.name:n/a}")
    private String appName;

    @GetMapping(value = "/", produces = "text/html")
    public String doHome() {
        StringBuilder html = new StringBuilder();
        html.append("<html>");
        html.append("<head>").append("<title>Azure Spring App : ").append(appName).append("</title>").append("</head>");
        html.append("<body bgcolor=\"#F1F3F4\">");
        html.append("Test API: <br>");
        html.append("<ul>");
        html.append(" <li><a href=\"/echo\">/echo</a></li>");
        html.append(" <li><a href=\"/config/vars\">/config/vars</a></li>");
        html.append(" <li><a href=\"/eureka/applications\">/eureka/applications</a></li>");
        html.append(" <li><a href=\"/eureka/applications/" + appName + "/next\">/eureka/applications/" + appName + "/next</a></li>");
        html.append(" <li><a href=\"/eureka/applications/{appName}/next\">/eureka/applications/{appName}/next</a></li>");
        html.append(" <li><a href=\"/eureka/applications/{appName}/next/echo\">/eureka/applications/{appName}/next/echo</a></li>");
        html.append(" <li><a href=\"/proxy/html?base64Url=aHR0cDovL2xvY2FsaG9zdDo4MDgxL2NvbmZpZy92YXJz\">debug: /proxy/html (put parameter \"base64Url\")</a></li>");
        html.append(" <li><a href=\"/proxy/json?base64Url=aHR0cDovL2xvY2FsaG9zdDo4MDgxL2NvbmZpZy92YXJz\">debug: /proxy/json (put parameter \"base64Url\")</a></li>");
        html.append("<ul>");
        html.append("<br>");
        html.append("</body>");
        html.append("</html>");
        return html.toString();
    }

    @GetMapping(value = "/echo", produces = "application/json")
    public String echo() {
        return "{\"whoami\" : \"" + appName + "\"}";
    }
}
