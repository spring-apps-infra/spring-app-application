package demo.app.http;

import java.io.Closeable;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

public final class HttpClient {

    private HttpClient() {
    }

    public static String get(String url) {
        CloseableHttpClient httpclient = null;
        CloseableHttpResponse httpresponse = null;
        InputStream is = null;
        try {
            httpclient = HttpClients.createDefault();
            httpresponse = httpclient.execute(new HttpGet(url));
            is = httpresponse.getEntity().getContent();
            return new String(is.readAllBytes(), StandardCharsets.UTF_8);

        } catch (Exception e) {
            return "error : " + getMessage(e);
        } finally {
            close(is);
            close(httpresponse);
            close(httpclient);
        }
    }

    private static String getMessage(Exception e) {
        if (e.getMessage() != null) {
            return e.getMessage();
        }
        if (e.getCause() != null && e.getCause().getMessage() != null) {
            return e.getCause().getMessage();
        }
        return e.getClass().getName();
    }

    private static void close(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
