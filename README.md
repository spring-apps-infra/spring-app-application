# Spring App Application
### Information
A very basic Springboot sample application

##### As part of series:
* Git repo for Config Server (used by Config Server)
* Spring App Config server (used by this project)
* Spring App Eureka server (used by this project)
* Spring App Cloud Gateway server (uses this project)
 
### Sample:
A simple Springboot application that:
- Loads its configurations from Config server
- Registers itself in Eureka server
- Exposes some sample API

```yaml
spring:
  application:
    name: spring-app-sample
  profiles:
    active: development
```

Using remote Config and Eureka servers

```
... -Dspring-boot.run.arguments="--config.server.protocol=https --config.server.host=<myConfigServerHost or default localhost> --config.server.port=<port or default 8888> --eureka.protocol=https --eureka.host=<myConfigServerHost or default localhost> --eureka.port=<port or default 8761>"
```

### API:
- GET /echo?test=Hello
- GET /config/vars
- GET /eureka/applications
- GET /eureka/applications/{appName}/next
- GET /eureka/applications/{appName}/next/echo
 
#### Debug API:
- GET /proxy/html?base64Url=<base 64 encoded URL>
- GET /proxy/json?base64Url=<base 64 encoded URL>